/* We would need 3 queries:
1.	Actlocal � transactional currency + local currency base for all BU's with ACTLOCAL
2.	Actuals � transactional currency + USD base (for BU�s with one ledger)
3.	Actuals � transactional currency (from Actlocal ledger) + USD base (for BU�s that have an Actlocal ledger)

Additional information required in the queries:
-	Journal source
-	Journal ID
-	Journal Line Description
-	Journal Header Description
-	User ID
-	Year
-	Period  */


/* 1.	Actlocal � transactional currency + local currency base for all BU's with ACTLOCAL */

select
'NEW'
, ' '
, a.journal_date
, 'CONVERSION'
, a.source
, b.foreign_currency as ""FOREIGN CUR""
, a.journal_date
, 'A'
, b.business_unit
, decode(B.OPERATING_UNIT,' ','000', B.OPERATING_UNIT) as ""LOCATION""
, decode(B.DEPTID,' ','0000',B.DEPTID) as ""DEPTID""
, b.account
, decode(B.CHARTFIELD1,' ','000',B.CHARTFIELD1) as ""DESTINATION""
, decode(B.PROJECT_ID,' ','00000000',B.PROJECT_ID) as ""AFE""
, decode(B.CHARTFIELD2,' ','0000000000',B.CHARTFIELD2) as ""REFERENCE""
, decode(B.AFFILIATE,' ','00000',B.AFFILIATE) as ""AFFILIATE""
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' ' as ""DEBIT""
,' ' as ""CREDIT""
,' ' as ""DEBIT""
,' ' as ""CREDIT""
,'PeopleSoft Conversion'
,' '
,' '
,'PeopleSoft Conversion'
, a.descr as ""Hdr Descr""
,' '
,' '
,' '
,' '
, b.line_descr
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
,' '
, a.journal_id
, a.oprid
, round(b.foreign_amount,2) as ""FOREIGN AMT""
, round(b.monetary_amount,2) as ""BASE AMT""
, b.currency_cd as ""BASE CUR""
, b.ledger
, a.fiscal_year
, a.accounting_period
--      , decode(A.ALTACCT,' ','000',A.ALTACCT)
from sysadm.ps_jrnl_header a, sysadm.ps_jrnl_ln b
where a.business_unit = b.business_unit
  and a.journal_id = b.journal_id
  and a.journal_date = b.journal_date
  and a.unpost_seq = b.unpost_seq
  and a.business_unit in
    ('AMC02','AUS01','CAH01','CRP01','CRP02','EMM01','EUR01','KOR01','MGL01','MHK01','MHT01','MJP01','MME01','MNZ01',
     'MSS01','MTL01','TMC02','UKL01','XAMS0','XCRS0','XEUS0','XMLS0','XMNS0','XTMS0','ZHKH0','ZKAI0','ZKMI0','ZMCH0')
  and a.fiscal_year = 2019
  and a.accounting_period in (1,2,3)
  and B.Ledger = 'ACTLOCAL'
  and a.jrnl_hdr_status in ('U','P')
  and b.account not like '9%'
  and b.monetary_amount <> 0.00
order by 8,1,2
;

